# Aplicación Spring Boot - Hola Mundo

Esta es una aplicación de ejemplo desarrollada en Spring Boot utilizando Java 11. La aplicación tiene un objetivo simple: mostrar un mensaje de "Hola Mundo".

## Requisitos Previos

Asegúrate de tener instalado lo siguiente en tu entorno de desarrollo:

- [Java Development Kit (JDK) 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Spring Boot](https://spring.io/projects/spring-boot)

## Ejecución de la Aplicación

1. Clona este repositorio en tu máquina local:

   ```bash
   git clone https://github.com/AleJCruz/helloworld

2. Navega al directorio de la aplicación:
./mvnw spring-boot:run

3. Abre un navegador y visita http://localhost:8080. Deberías ver el mensaje "Hola Mundo".

